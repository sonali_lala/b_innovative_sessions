# B_Innovative_Sessions

This repo contains the materials of the internal session conducted by the innovation lab. The topics are related to finance domain, machine learning, deep learning, and other technologies( aws, docker etc)

Domain
-------
Topics	-------------------------------	Authors
1. Capital Market----------------------	Lakshit
2. MacroEconomics----------------------	Pranesh

Machine Learning
----------------
Topics	-------------------------------	Authors
1. PCA	-------------------------------	Praveen
2. Logistic Regression ----------------	Indhumathi

DevOps
------
Topics	------------------------------- Authors
1. Docker  ---------------------------- Indhumathi
2. Aws Practitioner  ------------------	Sonali

Deep Learning
-------------
Topics -------------------------------	Authors
1. AI series maths&concepts ----------	Ankur
2. Intutive Deep Learning series -----  Sonali					